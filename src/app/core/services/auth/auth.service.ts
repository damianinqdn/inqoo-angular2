import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token = ''

  constructor() { }

  init() {

    const token = new URLSearchParams(window.location.hash).get('#access_token')
    if (token) {
      this.token = token
      sessionStorage.setItem('token', JSON.stringify(token))
    }
    else {
      this.token = JSON.parse(sessionStorage.getItem('token') || '""')
    }

    if (!this.token) {
      this.login()
    }
  }

  getToken() { return this.token }

  login() {
    const params = new URLSearchParams({
      client_id: 'b299cc1f5b694b6a929aa5090f5f48d9',
      response_type: 'token',
      redirect_uri: 'http://localhost:4200/'
    })
    const url = `https://accounts.spotify.com/authorize?${params}`


    window.location.href = url
  }

  logout() {

  }
}
