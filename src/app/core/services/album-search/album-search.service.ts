import { Injectable } from '@angular/core';
import { Album, AlbumSearchResponse } from '../../model/album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
// import { identifierModuleUrl } from '@angular/compiler';


interface AlbumResponse {
  albums: {
    items: Album[]
  }
}
@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {

  constructor(
    private http: HttpClient,
    private auth: AuthService) { }

  // 
  // holoyis165@bulkbye.com


  getAlbumById(id: string) {
    return this.http
      .get<Album>(`https://api.spotify.com/v1/albums/` + id, {
        headers: {
          'Authorization': 'Bearer ' + this.auth.getToken()
        }
      })
  }

  getAlbums(query: string) {
    return this.http
      .get<AlbumSearchResponse>(`https://api.spotify.com/v1/search`, {
        params: { q: query, type: 'album' },
        headers: {
          'Authorization': 'Bearer ' + this.auth.getToken()
        }
      }).pipe(
        map(resp => resp.albums.items),
        catchError(error => {
          if (!(error instanceof HttpErrorResponse)) {
            console.error(error);
            return throwError(new Error('Unexpected error'))
          }
          if (error.status == 401) {
            setTimeout(() => { this.auth.login() }, 2000)
            return throwError(new Error('Session expired. Logging you out'))
          }
          return throwError(new Error(error.error.error.message))
        })
      )
  }
}
