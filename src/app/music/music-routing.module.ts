import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailsViewComponent } from './containers/album-details-view/album-details-view.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { MusicComponent } from './music.component';

const routes: Routes = [{
  // app => music / ***
  path: '',
  component: MusicComponent,
  children: [
    {
      path: 'search',
      component: AlbumSearchComponent
    },
    {
      path: 'albums/:id',
      component: AlbumDetailsViewComponent
    },
    // {
    //   path: 'albums/:id',
    //   component: AlbumDetailsViewComponent
    // }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
