import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ResultsGridComponent } from './components/results-grid/results-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { FormsModule } from '@angular/forms';
import { AlbumSearchService } from '../core/services/album-search/album-search.service';
import { AlbumDetailsViewComponent } from './containers/album-details-view/album-details-view.component';
import { SharedModule } from '../share/share-module';

export const placki = 213

@NgModule({
  declarations: [
    MusicComponent,
    AlbumSearchComponent,
    SearchFormComponent,
    ResultsGridComponent,
    AlbumCardComponent,
    AlbumDetailsViewComponent
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers:[]
})
export class MusicModule { }
