import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';

@Component({
  selector: 'app-results-grid',
  templateUrl: './results-grid.component.html',
  styleUrls: ['./results-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush  // 
})
export class ResultsGridComponent implements OnInit {

  @Input()
  results:Album[] = []
  
  @Input()
  photos = [];

  constructor() { }

  ngOnInit(): void {
    console.log("Grid Results", this.results);
    
  }

}
