import { Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  @Input()
  result!:Album
  
  @Input()
  photos = [];

  constructor() { }

  ngOnInit(): void {
    if(!this.result) {throw "Missing Album Input"}
  }

}
