// import { Component, EventEmitter, OnInit, Output } from '@angular/core';


// @Component({
//   selector: 'app-search-form',
//   templateUrl: './search-form.component.html',
//   styleUrls: ['./search-form.component.scss']
// })
// export class SearchFormComponent implements OnInit {

//   name = ''

//   @Output()
//   searchEl = new EventEmitter()

//   constructor() { }

//   ngOnInit(): void {
//   }

//   clickButton(){
//     // console.log("test");
//     this.searchEl.emit(this.name)
    
//   }
// }

// Trener:

import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, FormGroupDirective, FormControlDirective, FormControlName, FormGroupName } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Output() search = new EventEmitter();

  // @Input() query = ''
  // ngOnChanges(changes: SimpleChanges): void {

  @Input() set query(query: string | null) {
    (this.searchForm.get('query') as FormControl).setValue(query, {
      emitEvent: false,
      onlySelf: true,
      emitViewToModelChange: false
    })
  }

  searchForm = new FormGroup({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    extras: new FormGroup({
      type: new FormControl('album'),
      placki: new FormControl('placki'),
    })
  })
  advOptions = false

  ngOnInit(): void {
    // this.searchForm.get('query')!.valueChanges.subscribe(console.log)

    this.searchForm.get('query')!.valueChanges.pipe(
      // not to often
      debounceTime(400),
      // not too short
      filter(q => q.length >= 3),
      // no duplicates
      distinctUntilChanged(),
    )
      // .subscribe({ next: val => this.search.emit(val) })
      // .subscribe({ next: val => this.search.next(val) })
      .subscribe(this.search)
  }

  constructor() {
    (window as any).form = this.searchForm
  }

  submit() {
    this.search.emit(
      this.searchForm.value.query)
  }


}
