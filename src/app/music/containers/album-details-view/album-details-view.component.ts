import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';
import { filter, map, switchMap } from 'rxjs/operators';
import { Album, AlbumSearchResponse } from 'src/app/core/model/album';
import { Observable } from 'rxjs';


/* 
  TODO:
  - load this on /music/albums?id=5Tby0U5VndHW0SomYO7Id7
  - show Id in html
  - fetch Alubm from server
  - show album Image, name
  - search results -> click card -> redirects here
*/

@Component({
  selector: 'app-album-details-view',
  templateUrl: './album-details-view.component.html',
  styleUrls: ['./album-details-view.component.scss']
})
export class AlbumDetailsViewComponent implements OnInit {
  

  ID$ = this.route.paramMap.pipe(
    map(param => param.get('id'))
  )

  album$ = this.ID$.pipe(
    filter((id): id is string => id != null),
    switchMap(id => this.service.getAlbumById(id))
  )

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: AlbumSearchService
  ) {
  }

  getId():void {
    // const query = this.route.snapshot.queryParamMap.get('id');
    // if(query) {
    //   this._id = query;
    // }
  }
    
  

  ngOnInit() {
    this.getId();
  }
}
