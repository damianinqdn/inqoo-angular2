import { Component, Inject, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Album } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';
import { __importDefault } from 'tslib';
import { map, filter, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {


  // results:Album[] = []
  message = '';
  SEARCH_PARAM = 'p';

  query = this.route.queryParamMap.pipe(
    map(params => params.get(this.SEARCH_PARAM)),
    filter((p): p is string => p !== null),
  )

  results = this.query.pipe(switchMap(query => this.service.getAlbums(query)),
    catchError((error) => {
      this.message = error.message
      return []
    })
  )

  constructor(
      // @Inject(AlbumSearchService)
      private route: ActivatedRoute,
      private router: Router,
      private service: AlbumSearchService
    ) {}

  
  // search(query: string) {
  //   this.service
  //     .getAlbums(query)
  //     .subscribe({
  //       next: (albums) => this.results = albums,
  //       error: (error) => this.message = error.message,
  //     })
  // }
  search(query: string) {
    this.router.navigate(['.'], {
      queryParams: { [this.SEARCH_PARAM]: query },
      relativeTo: this.route
    })
  }


  ngOnInit(): void {
    // this.results = this.service.getAlbums()
    console.log("test", this.results);
  }

}
