import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  size = 22;
  
  @Input()
  selected:any
  
  @Output()
  editItem = new EventEmitter()

  edit(){
    this.editItem.emit()
    console.log("edit");
  }
  // @Input()
  // playlist:Playlist = {
  //   id:'123',
  //   name:'Playlist 123',
  //   public:false,
  //   description:'my favourite playlist'
  // }
  
  constructor() { }

  ngOnInit(): void {
  }

}
