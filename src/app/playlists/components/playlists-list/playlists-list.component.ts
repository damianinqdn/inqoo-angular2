import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

  @Input('items')
  playlists : Playlist[] = []
  
  @Input()
  selected:any = ''

  @Output()
  selectedChange = new EventEmitter<Playlist>()
  
  select(item:Playlist) {
    console.log("select", item)
    this.selected = item
    this.selectedChange.emit(item)
  }

  constructor() { }

  ngOnInit(): void {
  }

  
}
