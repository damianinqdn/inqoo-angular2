import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit {

  @Input()
  playlist:Playlist = {
    id: '999',
    name: 'Playlist 234 lkjhlkjhlkjh',
    public: true,
    description: 'my favourite playlist'
  }

  draft:Playlist = {... this.playlist}
  

  @Output()
  cancelItem = new EventEmitter

  @Output()
  saveItem = new EventEmitter

  cancel(){
    console.log("Cancel");
    this.cancelItem.emit();
  }

  save(){
    console.log("Saving")
    this.saveItem.emit(this.draft);
  }
  constructor() { }

  ngOnInit(): void {
    // this.draft = {...this.playlist}
  }

}
