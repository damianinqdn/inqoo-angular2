import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: string = "details"
  
  playlists : Playlist[]= [{
    id: '123',
    name: 'Playlist 123',
    public: false,
    description: 'my favourite playlist'
  },
  {
    id:'234',
    name:'Playlist 234',
    public:false,
    description:'my favourite playlist'
  },
  {
    id:'456',
    name:'Playlist 345',
    public:false,
    description:'my favourite playlist'
  }]

  selected:any = this.playlists[0]

  edit() {
    console.log("edit");
    this.mode = "edit"
  }

  save(draft:Playlist) {
    console.log("saving");
    this.mode = "details";
    this.playlists.forEach(p => {
      if(p.id == draft.id) {
        p.name = draft.name;
        p.public = draft.public;
        p.description = draft.description;
      }
    });
  }

  cancel() {
    console.log("details");
    this.mode = "details"
  }
  constructor() { }

  ngOnInit(): void {

  }

}
