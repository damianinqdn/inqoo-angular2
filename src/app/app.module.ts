import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClockComponent } from './clock/clock.component';
import { CoreModule } from './core/core.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './share/share-module';

@NgModule({
  declarations: [ // componetns, directives, pipes..
    AppComponent, ClockComponent
  ],
  imports: [ // sub-modules
    BrowserModule,
    AppRoutingModule,
    PlaylistsModule,
    CoreModule,
  ],
  providers: [], // Services
  bootstrap: [
    AppComponent, ClockComponent /* ,HeaderComp, FooterComp.. */]
})
export class AppModule /* implements DoBootstrap */ {

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   // fetch json confg then....
  //   appRef.bootstrap(AppComponent, 'app-root')
  // }
}


// console.log(AppModule)