import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {

  // time:string = (new Date()).toLocaleTimeString();
  time?:string;

  constructor() {
    // @inject cdr: ChangeDetectorRef // turn off changes

   }

  ngOnInit(): void {
    setInterval(()=>{
      this.time = (new Date()).toLocaleTimeString();
    }) 
  }

}
